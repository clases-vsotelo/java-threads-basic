package com.vsotelo.example.runnables;

public class SendMessageRun implements Runnable {

    private Long id;
    private String to;
    private String message;

    static Object obj = new Object();

    public SendMessageRun(Long id, String to, String message) {
        this.id = id;
        this.to = to;
        this.message = message;
    }

    @Override
    public void run() {
        synchronized (obj) {
            System.out.println("/////////////////////////////////////////////////////////");
            System.out.println(" My Message Id is: " + id);
            System.out.println(" to : " + to);
            System.out.println(" Message is: " + message);
            System.out.println("/////////////////////////////////////////////////////////");
        }
    }
}