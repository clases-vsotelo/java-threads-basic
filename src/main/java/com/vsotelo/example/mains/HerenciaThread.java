package com.vsotelo.example.mains;

import java.util.ArrayList;
import java.util.List;

public class HerenciaThread extends Thread {

    private int id;
    static Object obj = new Object();

    @Override
    public void run() {
        synchronized (obj) {
            if (id == 6) {
                try {
                    throw new Exception("Error!");
                } catch (Exception e) {
                    System.out.println("error en 6");
                }
            } else {
                System.out.println("HerenciaThread Id = " + id);
            }
        }
    }

    public HerenciaThread(int id) throws Exception {
        this.id = id;
    }

    public static void main(String[] args) throws Exception {
        int cores = Runtime.getRuntime().availableProcessors();

        List<HerenciaThread> lstThreads = new ArrayList<>();

        for (int i = 0; i < cores; i++) {
            HerenciaThread herenciaThread1 = new HerenciaThread(i);
            lstThreads.add(herenciaThread1);
        }
        
        try {
            lstThreads.forEach(HerenciaThread::start);
            lstThreads.forEach(t -> {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            System.out.println("Se ejecuto correctamente!!");
        } catch (Exception e) {
            //System.out.println("Error !");
        }
        
    }
}