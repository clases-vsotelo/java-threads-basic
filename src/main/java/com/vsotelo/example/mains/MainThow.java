package com.vsotelo.example.mains;

import java.util.concurrent.TimeUnit;

public class MainThow {

    public static void main(String[] args) throws Exception {
        long _init = System.currentTimeMillis();

        Thread thread1 = new Thread(() -> {
            try {
                method1();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread thread2 = new Thread(() -> {
            try {
                method2();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        
        Thread thread3 = new Thread(() -> {
            try {
                method3();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        thread1.start();
        thread2.start();
        thread3.start();

        thread1.join();
        thread2.join();
        thread3.join();

        long _fin = System.currentTimeMillis();
        System.out.println("Duration ms -> " + (_fin - _init));
    }
    
    private static void method1() throws InterruptedException {
        System.out.println("Run method1");
        Thread.sleep(TimeUnit.SECONDS.toMillis(1));
    }

    private static void method2() throws InterruptedException {
        System.out.println("Run method2");
        Thread.sleep(TimeUnit.SECONDS.toMillis(1));
    }

    private static void method3() throws InterruptedException {
        System.out.println("Run method3");
        Thread.sleep(TimeUnit.SECONDS.toMillis(1));
    }
}