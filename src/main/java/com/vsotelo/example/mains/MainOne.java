package com.vsotelo.example.mains;

import java.util.concurrent.TimeUnit;

public class MainOne {

    public static void main(String[] args) throws Exception {
        long _init = System.currentTimeMillis();
        method1();
        method2();
        method3();
        long _fin = System.currentTimeMillis();
        System.out.println("Duration ms -> " + (_fin - _init));
    }

    private static void method1() throws InterruptedException {
        System.out.println("Run method1");
        Thread.sleep(TimeUnit.SECONDS.toMillis(1));
    }

    private static void method2() throws InterruptedException {
        System.out.println("Run method2");
        Thread.sleep(TimeUnit.SECONDS.toMillis(1));
    }

    private static void method3() throws InterruptedException {
        System.out.println("Run method3");
        Thread.sleep(TimeUnit.SECONDS.toMillis(1));
    }
}