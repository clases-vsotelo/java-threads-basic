package com.vsotelo.example.mains;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.vsotelo.example.runnables.SendMessageRun;

public class MainClass {

    public static void main(String[] args) {

        int numberProcesos = Runtime.getRuntime().availableProcessors();

        ExecutorService executor = Executors.newFixedThreadPool(numberProcesos);

        for (int i = 0; i < numberProcesos; i++) {
            SendMessageRun sm1 = new SendMessageRun(Long.parseLong(String.valueOf(i)), "Person" + i,
                    "This is a message example...");
            /*Thread thread = new Thread(sm1);
            thread.start();*/
            executor.execute(sm1);
        }
        executor.shutdown();
    }

}